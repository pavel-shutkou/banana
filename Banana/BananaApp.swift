//
//  BananaApp.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import SwiftUI

@main
struct BananaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
