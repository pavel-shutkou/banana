//
//  ContentView.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        WordCounterView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
