//
//  WordCounterViewModel.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Combine
import Foundation

final class WordCounterViewModel: WordCounterViewModeling {

    private let htmlWordCounter: HTMLWordCounterProtocol
    private let articleRetriever: ArticleRetrieverProtocol
    private let articleName: String
    private let isCaseSensitive: Bool

    private var subscriptions = Set<AnyCancellable>()

    init(articleName: String = "Banana",
         searchWord: String = "banana",
         isCaseSensitive: Bool = true,
         htmlWordCounter: HTMLWordCounterProtocol = WikipediaArticleWordsCounter(),
         articleRetriever: ArticleRetrieverProtocol = WikipediaServerManager())
    {
        self.htmlWordCounter = htmlWordCounter
        self.articleRetriever = articleRetriever
        self.articleName = articleName
        self.isCaseSensitive = isCaseSensitive
        super.init()
        self.serchWord = searchWord
    }

    override func countWords() {
        wordCount = 0
        isLoading = true
        articleRetriever.getArticleHTML(articleName: articleName)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: {
                [weak self] completion in
                    self?.isLoading = false
                    if case let .failure(error) = completion {
                        self?.errorString = error.localizedDescription
                        self?.shouldDisplayErrorAlert = true
                    }
            }, receiveValue: {
                [weak self] html in guard let self = self else { return }
                    self.isLoading = false
                    self.wordCount = self.htmlWordCounter.countWordInHTML(word: self.serchWord, in: html, isCaseSensitive: self.isCaseSensitive)
            })
            .store(in: &subscriptions)
    }

}
