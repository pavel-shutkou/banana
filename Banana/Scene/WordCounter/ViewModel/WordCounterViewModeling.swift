//
//  WordCounterViewModeling.swift
//  Banana
//
//  Created by Pavel on 28.01.22.
//

import Combine

class WordCounterViewModeling: ObservableObject {
    
    @Published var serchWord = ""
    @Published var wordCount = 0
    @Published var isLoading = false
    @Published var errorString: String? = nil
    @Published var shouldDisplayErrorAlert = false
    
    func countWords() {}
    
}
