//
//  WikipediaArticleWordsCounter.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Foundation

final class WikipediaArticleWordsCounter: HTMLWordCounterProtocol {

    func countWordInHTML(word: String, in html: String, isCaseSensitive: Bool = true) -> Int {
        let text = removeHTMLTags(from: html)
        return calculateWordCount(in: text, word: word, isCaseSensitive: isCaseSensitive)
    }

}

extension WikipediaArticleWordsCounter:  HTMLTagsRemoverProtocol, WordCounterProtocol {}
