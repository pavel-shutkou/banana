//
//  HTMLWordCounterProtocol.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Foundation

protocol HTMLWordCounterProtocol: AnyObject {
    
    func countWordInHTML(word: String, in html: String, isCaseSensitive: Bool) -> Int
    
}
