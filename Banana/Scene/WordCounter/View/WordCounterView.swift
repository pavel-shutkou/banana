//
//  WordCounterView.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import SwiftUI

struct WordCounterView: View {

    @ObservedObject var viewModel: WordCounterViewModeling
    
    init(viewModel: WordCounterViewModeling = WordCounterViewModel()) {
        self.viewModel = viewModel
    }

    var body: some View {
        VStack {
            Text("\"\(viewModel.serchWord)\" word counter")
                .fontWeight(/*@START_MENU_TOKEN@*/ .bold/*@END_MENU_TOKEN@*/)
                .font(/*@START_MENU_TOKEN@*/ .title/*@END_MENU_TOKEN@*/)
            Text("\(viewModel.wordCount)")
                .fontWeight(/*@START_MENU_TOKEN@*/ .bold/*@END_MENU_TOKEN@*/)
                .font(.system(size: 56.0))
                .padding(.top, 200)
            Button(action: {
                viewModel.countWords()
            }, label: {
                Text("Count")
                    .fontWeight(.bold)
                    .font(.title)
                    .padding()
                    .background(Color.purple)
                    .cornerRadius(40)
                    .foregroundColor(.white)
                    .padding(10)
                    .overlay(
                        RoundedRectangle(cornerRadius: 40)
                            .stroke(Color.purple, lineWidth: 5)
                    )
            })
            .disabled(viewModel.isLoading)

            .padding(.top, 80)
            .alert(isPresented: $viewModel.shouldDisplayErrorAlert) { () -> Alert in
                Alert(title: Text("Error"), message: Text("\(viewModel.errorString ?? "Network connection failure")"), dismissButton: .cancel())
            }
            Spacer()
            if viewModel.isLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle())
            }
        }
    }
    
}

struct WordCounterView_Previews: PreviewProvider {
    static var previews: some View {
        WordCounterView()
    }
}
