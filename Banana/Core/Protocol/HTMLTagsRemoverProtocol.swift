//
//  HTMLTagsRemoverProtocol.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Foundation

protocol HTMLTagsRemoverProtocol {

    func removeHTMLTags(from html: String) -> String

}

extension HTMLTagsRemoverProtocol {

    func removeHTMLTags(from html: String) -> String {
        return html.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }

}
