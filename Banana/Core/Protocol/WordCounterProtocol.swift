//
//  WordCounterProtocol.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Foundation

protocol WordCounterProtocol {

    func calculateWordCount(in text: String, word: String, isCaseSensitive: Bool) -> Int

}

extension WordCounterProtocol {

    func calculateWordCount(in text: String, word: String, isCaseSensitive: Bool) -> Int {
        var textString = text
        var wordString = word
        if !isCaseSensitive {
            textString = textString.lowercased()
            wordString = wordString.lowercased()
        }
        let wordsArray = textString.split { !$0.isLetter }
        let wordOccurrencesCount = wordsArray.filter { $0 == wordString }.count
        return wordOccurrencesCount
    }

}
