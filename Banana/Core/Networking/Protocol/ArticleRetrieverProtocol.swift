//
//  ArticleRetrieverProtocol.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Combine
import Foundation

protocol ArticleRetrieverProtocol: AnyObject {

    func getArticleHTML(articleName: String) -> AnyPublisher<String, URLError>

}
