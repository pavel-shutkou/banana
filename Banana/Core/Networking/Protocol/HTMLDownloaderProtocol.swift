//
//  HTMLDownloaderProtocol.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Combine
import Foundation

protocol HTMLDownloaderProtocol: AnyObject {

    func downloadHTML(url: URL) -> AnyPublisher<String, URLError>

}
