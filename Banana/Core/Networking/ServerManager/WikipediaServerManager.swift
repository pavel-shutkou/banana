//
//  WikipediaServerManager.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Foundation
import Combine

final class WikipediaServerManager: ArticleRetrieverProtocol {
    
    private let htmlDownloader: HTMLDownloaderProtocol
    
    init(htmlDownloader: HTMLDownloaderProtocol = HTMLDownloader()) {
        self.htmlDownloader = htmlDownloader
    }
    
    func getArticleHTML(articleName: String) -> AnyPublisher<String, URLError> {
        guard let url = URL(string: "https://en.wikipedia.org/wiki/\(articleName)"),
              !articleName.isEmpty else {
            return Fail(error: URLError(.badURL)).eraseToAnyPublisher()
        }
        return htmlDownloader.downloadHTML(url: url)
    }
    
}
