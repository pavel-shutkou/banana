//
//  HTMLServerManager.swift
//  Banana
//
//  Created by Pavel on 5.11.21.
//

import Combine
import Foundation

final class HTMLDownloader: HTMLDownloaderProtocol {

    func downloadHTML(url: URL) -> AnyPublisher<String, URLError> {
        URLSession.shared.dataTaskPublisher(for: url)
            .tryMap {
                guard let data = String(data: $0.data, encoding: .utf8) else {
                    throw URLError(.badServerResponse)
                }
                return data
            }
            .mapError { $0 as? URLError ?? URLError(.unknown) }
            .eraseToAnyPublisher()
    }

}
