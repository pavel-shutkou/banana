//
//  WikipediaServerManagerTests.swift
//  BananaTests
//
//  Created by Pavel on 28.01.22.
//

@testable import Banana
import Combine
import XCTest

final class WikipediaServerManagerTests: XCTestCase {
    
    private var cancellables = Set<AnyCancellable>()
    
    class HTMLDownloaderMock: HTMLDownloaderProtocol {
        
        var error: URLError?
        var html = "test"
        
        func downloadHTML(url: URL) -> AnyPublisher<String, URLError> {
            let subject = PassthroughSubject<String, URLError>()
            if let error = error {
                subject.send(completion: .failure(error))
            }
            else {
                subject.send("test")
                subject.send(completion: .finished)
            }
            return subject.eraseToAnyPublisher()
        }
    }
    
    func test_download() {
        let receivedAllValues = expectation(description: "all values received")
        let serverManager = WikipediaServerManager(htmlDownloader: HTMLDownloaderMock())
        serverManager.getArticleHTML(articleName: "banana")
            .sink(receiveCompletion: {
                comletion in
                switch comletion {
                case .finished:
                    break
                case .failure:
                    XCTFail("Error recieved")
                }
                receivedAllValues.fulfill()
            }, receiveValue: {
                htmlString in
                XCTAssertEqual(htmlString, "test")
            })
            .store(in: &cancellables)
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_errorThrown() {
        let receivedAllValues = expectation(description: "all values received")
        let HTMLDownloader = HTMLDownloaderMock()
        HTMLDownloader.error = URLError(.badURL)
        let serverManager = WikipediaServerManager(htmlDownloader: HTMLDownloader)
        serverManager.getArticleHTML(articleName: "banana")
            .sink(receiveCompletion: {
                comletion in
                switch comletion {
                case .finished:
                    XCTFail("No error")
                case .failure(let error):
                    XCTAssertEqual(error, URLError(.badURL))
                }
                receivedAllValues.fulfill()
            }, receiveValue: {
                _ in
                XCTFail("value with error")
            })
            .store(in: &cancellables)
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func test_emptyName() {
        let receivedAllValues = expectation(description: "all values received")
        let HTMLDownloader = HTMLDownloaderMock()
        let serverManager = WikipediaServerManager(htmlDownloader: HTMLDownloader)
        serverManager.getArticleHTML(articleName: "")
            .sink(receiveCompletion: {
                comletion in
                switch comletion {
                case .finished:
                    XCTFail("No error")
                case .failure(let error):
                    XCTAssertEqual(error, URLError(.badURL))
                }
                receivedAllValues.fulfill()
            }, receiveValue: {
                _ in
                XCTFail("value with error")
            })
            .store(in: &cancellables)
        waitForExpectations(timeout: 1, handler: nil)
    }
    
}
