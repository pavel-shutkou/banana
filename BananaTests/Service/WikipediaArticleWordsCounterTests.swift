//
//  WikipediaArticleWordsCounterTests.swift
//  BananaTests
//
//  Created by Pavel on 28.01.22.
//

@testable import Banana
import XCTest

final class WikipediaArticleWordsCounterTests: XCTestCase {

    func test_basicHTML() {
        let html = """
        <!DOCTYPE html>
        <html>
        <body>

        <h1>Banana</h1 Banana>
        <p>bananaBanana banana</p>

        </body>
        </html>

        """
        let wordsCounter = WikipediaArticleWordsCounter()
        XCTAssertEqual(wordsCounter.countWordInHTML(word: "banana", in: html), 1)
        XCTAssertEqual(wordsCounter.countWordInHTML(word: "Banana", in: html), 1)
    }

    func test_EmptyText() {
        let html = ""
        let wordsCounter = WikipediaArticleWordsCounter()
        XCTAssertEqual(wordsCounter.countWordInHTML(word: "banana", in: html), 0)
    }

    func test_EmptyWord() {
        let html = "sdsdfsdfsdfs"
        let wordsCounter = WikipediaArticleWordsCounter()
        XCTAssertEqual(wordsCounter.countWordInHTML(word: "", in: html), 0)
    }

}
