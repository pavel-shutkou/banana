//
//  HTMLTagsRemoverProtocolTests.swift
//  BananaTests
//
//  Created by Pavel on 28.01.22.
//

@testable import Banana
import XCTest

final class HTMLTagsRemoverProtocolTests: XCTestCase {
    
    class TagRemoverMock: HTMLTagsRemoverProtocol {}
    
    func test_basicHTML() {
        let html = """
        <!DOCTYPE html>
        <html>
        <body>

        <h1>test</h1><p>test</p>
        

        </body>
        </html>
        """
        XCTAssertEqual(TagRemoverMock().removeHTMLTags(from: html).trimmingCharacters(in: .whitespacesAndNewlines), "testtest")
    }
    
    func test_tagsTextIsRemoved() {
        let html = "<b sasdas sdfgfdgd>test<a/asdbf>"
        XCTAssertEqual(TagRemoverMock().removeHTMLTags(from: html), "test")
    }
    
    func test_emptyTag() {
        let html = "<b sasdas sdfgfdgd><a/asdbf>"
        XCTAssertEqual(TagRemoverMock().removeHTMLTags(from: html), "")
    }
    
    func test_withoutTags() {
        let html = "<test<"
        XCTAssertEqual(TagRemoverMock().removeHTMLTags(from: html), "<test<")
    }
    
    func test_incorrectTag() {
        let html = "test<>"
        XCTAssertEqual(TagRemoverMock().removeHTMLTags(from: html), "test<>")
    }
    
    func test_emptyHTML() {
        let html = ""
        XCTAssertEqual(TagRemoverMock().removeHTMLTags(from: html), "")
    }
    
}
