//
//  WordCounterProtocolTests.swift
//  BananaTests
//
//  Created by Pavel on 28.01.22.
//

@testable import Banana
import XCTest

final class WordCounterProtocolTests: XCTestCase {
    
    class WordCounterMock: WordCounterProtocol {}
    
    func test_someText() {
        let text = """
        A banana is an elongated, edible fruit – botanically a berry[1][2] – produced by several kinds of large herbaceous flowering plants in the genus Musa.[3] In some countries, bananas used for cooking may be called "plantains", distinguishing them from dessert bananas. The fruit is variable in size, color, and firmness, but is usually elongated and curved, with soft flesh rich in starch covered with a rind, which may be green, yellow, red, purple, or brown when ripe. The fruits grow upward in clusters near the top of the plant. Almost all modern edible seedless (parthenocarp) bananas come from two wild species – Musa acuminata and Musa balbisiana. The scientific names of most cultivated bananas are Musa acuminata, Musa balbisiana, and Musa × paradisiaca for the hybrid Musa acuminata × M. balbisiana, depending on their genomic constitution. The old scientific name for this hybrid, Musa sapientum, is no longer used.
        """
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: text, word: "Banana", isCaseSensitive: false), 1)
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: text, word: "Banana", isCaseSensitive: true), 0)
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: text, word: "The", isCaseSensitive: true), 4)
    }
    
    func test_emptyText() {
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: "", word: "Banana", isCaseSensitive: false), 0)
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: "", word: "Banana", isCaseSensitive: true), 0)
    }
    
    func test_oneWord() {
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: "banana", word: "Banana", isCaseSensitive: false), 1)
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: "banana", word: "Banana", isCaseSensitive: true), 0)
    }
    
    func test_emptyWord() {
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: "banana", word: "", isCaseSensitive: false), 0)
        XCTAssertEqual(WordCounterMock().calculateWordCount(in: "banana", word: "", isCaseSensitive: true), 0)
    }
    
}
